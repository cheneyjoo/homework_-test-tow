package questionthree.usecase.base;

/**
 * 套餐类
 */
public abstract class BaseSetMeal {
    private String setMealName;
    private String setMealType;
    private String monthlyRates;

    public String getSetMealName() {
        return setMealName;
    }

    public void setSetMealName(String setMealName) {
        this.setMealName = setMealName;
    }

    public String getSetMealType() {
        return setMealType;
    }

    public void setSetMealType(String setMealType) {
        this.setMealType = setMealType;
    }

    public String getMonthlyRates() {
        return monthlyRates;
    }

    public void setMonthlyRates(String monthlyRates) {
        this.monthlyRates = monthlyRates;
    }
    public abstract void Show();
}
