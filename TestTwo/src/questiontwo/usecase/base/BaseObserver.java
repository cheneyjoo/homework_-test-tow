package questiontwo.usecase.base;

import questiontwo.usecase.impl.Scene;

public abstract class BaseObserver {
    private int playStatus;//游戏当前状态

    public int get_playStatus() {
        return playStatus;
    }
    public void set_playStatus(int _playStatus) {
        this.playStatus = _playStatus;
    }

    public static BaseScene scene = Scene.GetInstance();//场景类对象，
    public abstract boolean CheckDropOff(String point);//检验落子是否有效
    public abstract void UpdatePiece(String point, String Piece);//更新棋盘棋子摆布
    public abstract boolean IsVictory(String point, String Piece);//判断是否有棋手取的胜利
    public abstract void Start(BaseChessPlayer player1, BaseChessPlayer player2);//开始游戏
    public abstract void Stop();//结束游戏
}
