package questiontwo.usecase.impl;

import questiontwo.usecase.base.BaseScene;

/**
 * 场景类
 */
public class Scene extends BaseScene {
    //单例模式，静态对象
    private static Scene scene = new Scene();
    //初始化棋盘内容
    private Scene() {
        for (int i = 0; i < Piece.length; i++) {
            for (int j = 0; j < Piece[i].length; j++) {
                Piece[i][j] = "+";
            }
        }
    }
    /**
     * 绘制棋盘
     */
    @Override
    public void DrawCheckerBoard() {
        System.out.print("    ");
        for (int i = 0; i < Piece[0].length; i++) {
            if (i > 9) {
                System.out.print((char) +('a' + (i - 10)) + "   ");
            } else {
                System.out.print(i + "   ");
            }
        }
        System.out.println();
        for (int i = 0; i < Piece.length; i++) {
            if (i > 9) {
                System.out.print((char) +('a' + (i - 10)) + "   ");
            } else {
                System.out.print(i + "   ");
            }
            for (int j = 0; j < Piece[i].length; j++) {
                System.out.print(Piece[i][j] + "   ");
            }
            System.out.println();
        }
    }
    //单例模式获取场景对象
    public static Scene GetInstance() {
        return scene;
    }
}
