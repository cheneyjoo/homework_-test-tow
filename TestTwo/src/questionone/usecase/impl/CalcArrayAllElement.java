package questionone.usecase.impl;

import questionone.usecase.base.BaseCalculationArrayValue;

public class CalcArrayAllElement extends BaseCalculationArrayValue {
    @Override
    public void Handler() {
        int totalValue = 0;
        for (int i = 0; i < Array.length; i++)
        {
            for (int j = 0; j < Array[i].length; j++)
            {
                totalValue += Array[i][j];
            }
        }
        System.out.println("计算数组全部行列值为："+ totalValue);
        if (getNextHandler() != null) {
            nextHandler.Handler();
        }
    }
}
