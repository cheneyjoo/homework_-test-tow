package questionthree.usecase.impl;

import questionthree.usecase.base.BaseConsumptionInformation;
import questionthree.usecase.base.BaseMobileCard;

public class ConsumptionInformation extends BaseConsumptionInformation {
    @Override
    public void Show(BaseMobileCard mobileCard, BaseConsumptionInformation consumptionInformation) {
        System.out.println("----------------本卡信息-----------------------");
        System.out.println("卡号：" + mobileCard.getCardNo());
        System.out.println("账户名：" + mobileCard.getUserName());
        System.out.println("服务密码：" + mobileCard.getPassWord());
        System.out.println("卡类型：" + mobileCard.getMobileCardType().getCardType());
        System.out.println("账户余额：" + mobileCard.getAccountBalance());
        System.out.println("通话时长(分钟)：" + mobileCard.getTalkTime());
        System.out.println("上网流量：" + mobileCard.getNetFlow());
        System.out.println("当月统计通话时长：" + consumptionInformation.getTotalTalkMinute()+"分钟");
        System.out.println("当月统计上网流量：" + consumptionInformation.getTotalNetFlow()+"GB");
        System.out.println("当月消费金额：" + consumptionInformation.getMonthlyConsumptionAmount()+"元");
    }
}
