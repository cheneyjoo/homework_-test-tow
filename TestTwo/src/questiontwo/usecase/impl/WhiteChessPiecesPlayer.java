package questiontwo.usecase.impl;

import questiontwo.usecase.base.BaseChessPlayer;

/**
 * 白棋玩家
 */
public class WhiteChessPiecesPlayer extends BaseChessPlayer {
    public WhiteChessPiecesPlayer(String playerName)
    {
        this.setPlayerName(playerName);
        this.setPiecesColor("○");
    }
}
