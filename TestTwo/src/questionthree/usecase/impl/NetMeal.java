package questionthree.usecase.impl;

import questionthree.usecase.base.BaseMobileCard;
import questionthree.usecase.base.BaseSetMeal;
import questionthree.usecase.service.INetService;

/**
 * 上网套餐类 上网流量
 */
public class NetMeal extends BaseSetMeal implements INetService {
    private int netFlow;

    public int getNetFlow() {
        return netFlow;
    }

    public void setNetFlow(int netFlow) {
        this.netFlow = netFlow;
    }

    @Override
    public void Show() {
        System.out.println("套餐名称：" + this.getSetMealName() + ",套餐类型：" + this.getSetMealType() + ",每月资费：" + this.getMonthlyRates() + ",上网流量：" + this.getNetFlow());
    }

    @Override
    public void NetFlow(int netFlow, BaseMobileCard mobileCard) {
        System.out.println("用户：" + mobileCard.getUserName() + ",卡号：" + mobileCard.getCardNo() + "进行了上网服务，消耗流量：" + netFlow + "KB");
    }
}
