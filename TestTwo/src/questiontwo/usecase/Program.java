package questiontwo.usecase;

import questiontwo.usecase.base.BaseChessPlayer;
import questiontwo.usecase.base.BaseObserver;
import questiontwo.usecase.base.BaseScene;
import questiontwo.usecase.impl.BlackChessPiecesPlayer;
import questiontwo.usecase.impl.Observer;
import questiontwo.usecase.impl.Scene;
import questiontwo.usecase.impl.WhiteChessPiecesPlayer;

public class Program {
    public static void main(String[] args) {
        BaseScene scene = Scene.GetInstance();
        scene.DrawCheckerBoard();
        BaseChessPlayer player1 = new BlackChessPiecesPlayer("江流儿");
        BaseChessPlayer player2 = new WhiteChessPiecesPlayer("黑木");
        BaseObserver observer = Observer.GetInstance();
        observer.Start(player1, player2);
    }
}
