package questiontwo.usecase.impl;
import questiontwo.usecase.base.BaseChessPlayer;
/**
 * 黑棋玩家
 */
public class BlackChessPiecesPlayer extends BaseChessPlayer {
    public BlackChessPiecesPlayer(String playerName)
    {
        this.setPlayerName(playerName);
        this.setPiecesColor("●");
    }
}
