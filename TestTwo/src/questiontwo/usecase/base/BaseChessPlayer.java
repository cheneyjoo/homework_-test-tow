package questiontwo.usecase.base;

import questiontwo.usecase.impl.Observer;

import java.util.Scanner;

/**
 * 抽象棋手类
 */
public abstract class BaseChessPlayer {
    private String playerName;//棋手姓名
    private String piecesColor;//棋子颜色

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPiecesColor() {
        return piecesColor;
    }

    public void setPiecesColor(String piecesColor) {
        this.piecesColor = piecesColor;
    }
    //添加观察者裁判对象
    private Observer observer = Observer.GetInstance();
    //下棋
    public void PlayChess() {
        while (true) {
            Scanner scan = new Scanner(System.in);
            if (scan.hasNext()) {
                String point = scan.next();
                boolean flag = observer.CheckDropOff(point);
                if (flag) {
                    observer.UpdatePiece(point, piecesColor);
                    if (observer.IsVictory(point, piecesColor)) {
                        System.out.println(this.getPlayerName() + "胜利！！");
                        observer.Stop();
                    }
                    break;
                }
            }
        }
    }
}
