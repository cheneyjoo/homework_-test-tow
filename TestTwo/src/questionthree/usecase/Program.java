package questionthree.usecase;

import questionthree.usecase.base.BaseConsumptionInformation;
import questionthree.usecase.base.BaseMobileCard;
import questionthree.usecase.base.BaseSetMeal;
import questionthree.usecase.base.MobileCardType;
import questionthree.usecase.impl.ConsumptionInformation;
import questionthree.usecase.impl.MobileCard;
import questionthree.usecase.impl.NetMeal;
import questionthree.usecase.impl.TalkMeal;
import questionthree.usecase.service.INetService;
import questionthree.usecase.service.ITalkService;

public class Program {
    public static void main(String[] args) {
        /*
        * 初始化一张卡
        * */
        BaseMobileCard mobileCard=new MobileCard();
        mobileCard.setCardNo("13800138000");
        mobileCard.setUserName("张三");
        mobileCard.setPassWord("9408251");
        mobileCard.setMobileCardType(MobileCardType.Little);
        mobileCard.setAccountBalance(1000);
        mobileCard.setNetFlow(100);
        mobileCard.Show();
        System.out.println("----------------------------------------------------------");
        /*
        * 模拟电话卡绑定套餐消费服务
        * */
        TalkMeal setTalkMeal=new TalkMeal();
        setTalkMeal.setSetMealName("全国免费通话500分钟套餐");
        setTalkMeal.setSetMealType("通话套餐");
        setTalkMeal.setMonthlyRates("98元");
        setTalkMeal.setTalkMinute(500);
        setTalkMeal.setMessageCount(50);
        setTalkMeal.Show();
        System.out.println("----------------------------------------------------------");
        NetMeal setNetMeal=new NetMeal();
        setNetMeal.setSetMealName("全国不限量200G任意玩套餐");
        setNetMeal.setSetMealType("流量套餐");
        setNetMeal.setMonthlyRates("198元");
        setNetMeal.setNetFlow(200);
        setNetMeal.Show();
        /**
         * 模拟电话卡消费服务
         */
        ITalkService talkService=new TalkMeal();
        talkService.TalkMinute(10,mobileCard);
        System.out.println("----------------------------------------------------------");
        INetService netService=new NetMeal();
        netService.NetFlow(2000,mobileCard);
        System.out.println("----------------------------------------------------------");
        /**
         * 模拟打印账单服务
         */
        BaseConsumptionInformation consumptionInformation=new ConsumptionInformation();
        consumptionInformation.setMonthlyConsumptionAmount(300);
        consumptionInformation.setTotalNetFlow(100);
        consumptionInformation.setTotalTalkMinute(50);
        consumptionInformation.Show(mobileCard,consumptionInformation);
    }
}
