package questionone.usecase.base;

/**
 * 申明计算数组值抽象类
 */
public abstract class BaseCalculationArrayValue {
    /**
     * 申明静态公共数组变量
     */
    public static int[][] Array = new int[16][16];
    /**
     * 下一个处理对象，提供链式处理
     */
    protected BaseCalculationArrayValue nextHandler;
    /**
     * 获取下一级处理对象
     * @return 下一级处理对象
     */
    public BaseCalculationArrayValue getNextHandler() {
        return nextHandler;
    }

    /**
     * 设置下一级处理对象
     * @param nextHandler
     */
    public void setNextHandler(BaseCalculationArrayValue nextHandler)
    {
        this.nextHandler = nextHandler;
    }
    /**
     * 构造方法为公共数组初始化值
     */
    public BaseCalculationArrayValue() {
        for (int i = 0; i < Array.length; i++) {
            for (int j = 0; j < Array[i].length; j++) {
                Array[i][j] = (i + 1) * (j + 1);
            }
        }
    }
    /**
     * 抽象处理方法
     */
    public abstract void Handler();
}