package questionone.usecase.impl;

import questionone.usecase.base.BaseCalculationArrayValue;

public class CalcArrayFromRightToLeft extends BaseCalculationArrayValue {
    @Override
    public void Handler() {
        int totalValue = 0;
        for (int i = 0; i < Array.length; i++) {

            totalValue += Array[i][Array[i].length - (i + 1)];

        }
        System.out.println("计算数组右上角到左下角全部行列值为：" + totalValue);
        if (getNextHandler() != null) {
            nextHandler.Handler();
        }
    }
}
