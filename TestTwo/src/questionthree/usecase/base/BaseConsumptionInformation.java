package questionthree.usecase.base;

/**
 * 用户消费信息类
 * 统计通话时长、统计上网流量、每月消费金额
 */
public abstract class BaseConsumptionInformation {
    private int totalTalkMinute;
    private double totalNetFlow;
    private double monthlyConsumptionAmount;

    public int getTotalTalkMinute() {
        return totalTalkMinute;
    }

    public void setTotalTalkMinute(int totalTalkMinute) {
        this.totalTalkMinute = totalTalkMinute;
    }

    public double getTotalNetFlow() {
        return totalNetFlow;
    }

    public void setTotalNetFlow(double totalNetFlow) {
        this.totalNetFlow = totalNetFlow;
    }

    public double getMonthlyConsumptionAmount() {
        return monthlyConsumptionAmount;
    }

    public void setMonthlyConsumptionAmount(double monthlyConsumptionAmount) {
        this.monthlyConsumptionAmount = monthlyConsumptionAmount;
    }

    public abstract void Show(BaseMobileCard mobileCard, BaseConsumptionInformation consumptionInformation);
}
