package questionthree.usecase.impl;

import questionthree.usecase.base.BaseMobileCard;

/**
 * 手机卡
 */
public class MobileCard extends BaseMobileCard {
    @Override
    public void Show() {
        System.out.println("----------------本卡信息-----------------------");
        System.out.println("卡号：" + this.getCardNo());
        System.out.println("账户名：" + this.getUserName());
        System.out.println("服务密码：" + this.getPassWord());
        System.out.println("卡类型：" + this.getMobileCardType().getCardType());
        System.out.println("账户余额：" + this.getAccountBalance());
        System.out.println("通话时长(分钟)：" + this.getTalkTime());
        System.out.println("上网流量：" + this.getNetFlow());
    }
}
