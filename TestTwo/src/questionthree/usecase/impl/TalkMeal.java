package questionthree.usecase.impl;

import questionthree.usecase.base.BaseMobileCard;
import questionthree.usecase.base.BaseSetMeal;
import questionthree.usecase.service.ITalkService;

/**
 * 通话套餐 特征：通话时长、短信条数、每月资费 行为: 显示所有套餐信息
 */
public class TalkMeal extends BaseSetMeal implements ITalkService {
    private int talkMinute;
    private int messageCount;

    public int getTalkMinute() {
        return talkMinute;
    }

    public void setTalkMinute(int talkMinute) {
        this.talkMinute = talkMinute;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    @Override
    public void Show() {
        System.out.println("套餐名称：" + this.getSetMealName() + ",套餐类型：" + this.getSetMealType() + ",每月资费：" + this.getMonthlyRates() + ",通话时长：" + this.getTalkMinute() + ",短信条数：" + this.getMessageCount());
    }

    @Override
    public void TalkMinute(int minute, BaseMobileCard mobileCard) {
        System.out.println("用户：" + mobileCard.getUserName() + ",卡号：" + mobileCard.getCardNo() + "进行了通话服务，通话时长：" + minute + "分钟");
    }
}
