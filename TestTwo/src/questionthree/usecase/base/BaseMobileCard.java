package questionthree.usecase.base;

/**
 * 手机卡类 特征：卡类型、卡号、用户名、密码、账户余额、通话时长(分钟)、上网流量 行为：显示（卡号 + 用户名 + 当前余额）
 */
public abstract class BaseMobileCard {
    private MobileCardType mobileCardType;
    private String cardNo;
    private String userName;
    private String passWord;
    private double accountBalance;
    private int talkTime;
    private double netFlow;

    public MobileCardType getMobileCardType() {
        return mobileCardType;
    }

    public void setMobileCardType(MobileCardType mobileCardType) {
        this.mobileCardType = mobileCardType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(int talkTime) {
        this.talkTime = talkTime;
    }

    public double getNetFlow() {
        return netFlow;
    }

    public void setNetFlow(double netFlow) {
        this.netFlow = netFlow;
    }
    public abstract void Show();
}
