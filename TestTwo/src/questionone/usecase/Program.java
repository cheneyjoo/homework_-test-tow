package questionone.usecase;

import questionone.usecase.base.BaseCalculationArrayValue;
import questionone.usecase.impl.*;

public class Program {
    public static void main(String[] args) {
        //初始化计算对象
        BaseCalculationArrayValue allValue = new CalcArrayAllElement();
        BaseCalculationArrayValue leftToRight = new CalcArrayFromLeftToRight();
        BaseCalculationArrayValue rightToLeft = new CalcArrayFromRightToLeft();
        //设置链式处理顺序
        allValue.setNextHandler(leftToRight);
        leftToRight.setNextHandler(rightToLeft);
        //开始链式计算
        allValue.Handler();
    }
}
