package questionthree.usecase.base;

public enum MobileCardType {

    Big("大卡"), Small("小卡"), Little("微卡");
    private String cardType;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    private MobileCardType(String value) {
        this.cardType = value;
    }
}
