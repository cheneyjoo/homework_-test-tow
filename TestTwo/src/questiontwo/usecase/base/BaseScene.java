package questiontwo.usecase.base;

/**
 * 抽象场景类
 */
public abstract class BaseScene {
    //静态全局棋子数组对象
    public static String[][] Piece = new String[16][16];
    //抽象绘制棋盘方法
    public abstract void DrawCheckerBoard();
}
