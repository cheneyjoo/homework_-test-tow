package questionthree.usecase.service;

import questionthree.usecase.base.BaseMobileCard;

public interface ITalkService {
    void TalkMinute(int minute, BaseMobileCard mobileCard);
}
