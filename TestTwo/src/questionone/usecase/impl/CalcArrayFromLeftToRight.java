package questionone.usecase.impl;

import questionone.usecase.base.BaseCalculationArrayValue;

public class CalcArrayFromLeftToRight extends BaseCalculationArrayValue {
    @Override
    public void Handler() {
        int totalValue = 0;
        for (int i = 0; i < Array.length; i++)
        {
            totalValue += Array[i][i];
        }
        System.out.println("计算数组左上角到右下角全部行列值为：" + totalValue);
        if (getNextHandler() != null)
        {
            nextHandler.Handler();
        }
    }
}
