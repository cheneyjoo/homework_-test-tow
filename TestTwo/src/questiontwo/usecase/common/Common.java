package questiontwo.usecase.common;

public class Common {
    public static Integer TryParse(String value) {
        try {
            Integer number = Integer.parseInt(value);
            return number;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
